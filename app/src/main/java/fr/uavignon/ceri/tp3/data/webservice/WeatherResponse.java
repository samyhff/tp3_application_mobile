package fr.uavignon.ceri.tp3.data.webservice;


public class WeatherResponse {

    public Main main;
    public Wind wind;
    public Clouds clouds;
    public Weather weather;

    public static class Weather {
        public String description;
    }

    public static class Main {
        public float temp;
        public int humidity;

    }

    public static class Wind {
        public int speed;
        public int deg;
    }

    public static class Clouds {
        public Integer all=null;
        }
}



package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {

    @Headers("Accept: application/json; charset=utd-8")
    @GET("/data/2.5/weather")
    Call<WeatherResponse> getForecast(@Query("q") String query,
        @Query("APIkey") String apiKey);

}

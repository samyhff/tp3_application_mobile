package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setDescription(weatherInfo.weather.description);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
    }
}
